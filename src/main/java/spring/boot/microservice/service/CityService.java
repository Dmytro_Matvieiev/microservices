package spring.boot.microservice.service;

import spring.boot.microservice.domain.City;

import java.util.List;
import java.util.Optional;

public interface CityService {
    List<City> findAll();
    Optional<City> findById(Long id);
}
