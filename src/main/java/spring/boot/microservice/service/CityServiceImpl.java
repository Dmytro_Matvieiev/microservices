package spring.boot.microservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spring.boot.microservice.domain.City;
import spring.boot.microservice.repository.CityRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public List<City> findAll() {
        return cityRepository.findAll();
    }

    @Override
    public Optional<City> findById(Long id) {
        return cityRepository.findById(id);
    }
}
