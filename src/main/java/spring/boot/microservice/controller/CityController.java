package spring.boot.microservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import spring.boot.microservice.domain.City;
import spring.boot.microservice.service.CityService;

import java.util.List;
import java.util.Optional;

@RestController
public class CityController {

    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("/cities")
    public List<City> findAllCities() {
        return cityService.findAll();
    }

    @GetMapping("/city/{id}")
    public Optional<City> findCityById(@PathVariable Long id) {
        return cityService.findById(id);
    }
}
