package spring.boot.microservice.actuator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class HealthCheck implements HealthIndicator {

    private String appStatus = "Available";
    @Value("${info.app.name}")
    private String appName;
    @Value("${info.app.description}")
    private String appDescription;
    @Value("${info.app.version}")
    private String appVersion;

    private Map<String, String> map = new LinkedHashMap<>();

    @Override
    public Health health() {
        map.put("appStatus", appStatus);
        map.put("appName", appName);
        map.put("appDescription", appDescription);
        map.put("appVersion", appVersion);

        if (!isRunning()) {
            return Health.down().withDetail("app", "Not Available").build();
        }
        return Health.up().withDetails(map).build();
    }
    private Boolean isRunning() {
        return true;
    }
}


