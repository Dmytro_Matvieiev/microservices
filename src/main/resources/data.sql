DROP TABLE IF EXISTS city;

CREATE TABLE city (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL UNIQUE,
  number_of_districts INT NOT NULL
);

INSERT INTO city (name, number_of_districts) VALUES
  ('Kiev', 15),
  ('Kharkiv', 10),
  ('Lviv', 9),
  ('Poltava', 6),
  ('Ternopil', 9),
  ('Sumy', 6),
  ('Nikolaev', 7),
  ('Mariupol', 6),
  ('Dnipro', 8),
  ('Odessa', 10);